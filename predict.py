import pandas as pd
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.metrics import mean_squared_error
from sklearn.neighbors import KNeighborsRegressor
from sklearn import tree
import matplotlib.pyplot as plt
import time

def calc(path, percent):
    data = pd.read_csv(path)
    features = ["latitude", "longitude", "depth", ]
    X = data[features]
    y = data["mag"]
    percent = percent/100
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=percent, random_state=22)
    neigh = KNeighborsRegressor(n_neighbors=3)
    neigh.fit(X_train, y_train)
    y_pred = neigh.predict(X_test)
    mse = mean_squared_error(y_test, y_pred)
    return mse, y_pred, neigh, y_test.values

def data_plot(plot_type,path):
    data = pd.read_csv(path)
    if plot_type == 'Магнитуда':
        return data['mag']
    elif plot_type == 'Глубина':
        return data['depth']
    else:
        return 'None'

def pred(lat,long,depth,neigh):
    res = neigh.predict([[lat,long,depth]])
    return res
