# Прецедентный анализ

Проект сделан при помощи:
- Flask
- Sklearn
- Pandas
- Matplotlib

Разработчик: Малашихин Дмитрий, ИСм-1
 
themdq@yandex.ru 👋🏻

### Действия для запуска программы

- Склонируйте репозиторий

`git clone https://gitlab.com/themdq/prediction.git`
- Зайдите в папку с проектом

`cd predict`
- Создайте окружение

`python -m venv .venv`
- Установите зависимости проекта

`pip install -r requirements.txt`
- Запустите приложение

`python app.py`
