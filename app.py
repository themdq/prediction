from flask import Flask, render_template, request, url_for, flash, redirect, jsonify
import flask
import pickle


# App definition
app = Flask(__name__, template_folder='templates')

# importing models
with open('/Users/themdq/Desktop/model_v1.pk', 'rb') as f:
    classifier = pickle.load(f)


@app.route('/')
def welcome():
    return render_template('index.html')

#6,148.0,72.0,35.0,30.5,33.6,0.627,50

@app.route('/', methods=['POST', 'GET'])
def predict():
    if flask.request.method == 'GET':
        return "Prediction page"

    if flask.request.method == 'POST':
        x=[]
        for i in range(1,9):
            title = request.form[f'title{i}']
            x.append(title)
        prediction = list(classifier.predict([x]))
        print(prediction[0])
        if prediction[0] == 0:
            data = 'Вы не в зоне риска заболевания'
        elif prediction[0] == 1:
            data = 'Вы в зоне риска заболевания! Срочно обратитесь к врачу'
        else:
            data = 'Ошибка'
        return render_template('prediction.html', data=data)


if __name__ == "__main__":
    app.run()